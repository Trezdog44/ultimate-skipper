<h1 align="center">Ultimate Skipper</h1>
<p align="center">Automatically skip Anime Openings and Endings.</p>
<p align="center"><a href="#readme"><img src="https://gitlab.com/Arisa_Snowbell/ultimate-skipper/-/raw/master/preview.png?inline=true" alt="Preview"/></a></p>

## Installation

Download the [src/ultimate-skipper.lua](https://gitlab.com/Arisa_Snowbell/ultimate-skipper/-/raw/master/src/ultimate-skipper.lua?inline=false) file and place it in
your VLC extensions directory:

- Linux:
    - (All Users)
      - `/usr/lib/vlc/lua/extensions/`
    - (Current User):
      - `~/.local/share/vlc/lua/extensions/`
- MacOS:
    - (All Users)
       - `/Applications/VLC.app/Contents/MacOS/share/lua/extensions/`
    - (Current User):
       - `/Users/<name>/Library/Application Support/org.videolan.vlc/lua/extensions/`
- Windows:
    - (All Users)
      - `%ProgramFiles%\VideoLAN\VLC\lua\extensions\`
    - (Current User):
      - `%APPDATA%\VLC\lua\extensions\`

## Usage

1. From the <kbd>View</kbd> menu, select <kbd>Auto Skip Openings/Endings</kbd>.
2. Set times for openings and endings and then profile name which is used to compare to the name of the video. (Basically, if video name contains profile name)
3. Profiles are saved as a file named `ultimate-skipper.conf` in your VLC [config
   directory][config-dir].
4. Then close the dialog and start to watch.

Reminder: Every time you open VLC you must go to View and click this plugin and close the dialog. VLC doesn't allow it to start automatically.
Always when interacting(Activating, Deactivating, Saving and Loading profiles...) with this extension stop the video.

## Contributing

Bug reports and pull requests are welcome on [GitLab][gitlab].

## License

This project is available under the terms of the MIT license. See the
[`LICENSE`](LICENSE) file for the copyright information and licensing terms.

[gitlab]: https://gitlab.com/Arisa_Snowbell/ultimate-skipper
[config-dir]: https://www.videolan.org/support/faq.html#Config
