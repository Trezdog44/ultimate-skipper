--[[
-- Installation:
-- Linux (All Users): /usr/lib/vlc/lua/extensions/
-- Linux (Current User): ~/.local/share/vlc/lua/extensions/
-- MacOS (All Users): /Applications/VLC.app/Contents/MacOS/share/lua/extensions/
-- MacOS (Current User): /Users/<name>/Library/Application Support/org.videolan.vlc/lua/extensions/
-- Windows (All Users): %ProgramFiles%\VideoLAN\VLC\lua\extensions\
-- Windows (Current User): %APPDATA%\VLC\lua\extensions\
-- Profiles are saved in: ~/.config/vlc/ultimate-skipper.conf
--]]
local active = false;
function descriptor()
    return {
        title = "Ultimate Skipper",
        version = "0.6.6",
        author = "Arisa Snowbell",
        url = "https://gitlab.com/Arisa_Snowbell/ultimate-skipper",
        shortdesc = "Auto Skip Openings/Endings",
        description = "Automatically skip Anime Openings and Endings.",
        capabilities = {"playing-listener"}
    }
end

function activate(script)
    script = script or false
    if active == false then
        active = true
        profiles = {}
        config_file = vlc.config.configdir() .. "/ultimate-skipper.conf"

        if (file_exists(config_file)) then
            load_all_profiles()
        end
    end
    if script == false then
        open_dialog()
        playing_changed()
    end
end

function deactivate()
    active = false
    dialog:delete()
end

function close()
    dialog:hide()
end

function meta_changed()
end

function open_dialog()
    dialog = vlc.dialog(descriptor().title)

    dialog:add_label("<center><h3>Profile</h3></center>", 1, 1, 2, 1)
    dialog:add_button("Load", populate_profile_fields, 1, 3, 1, 1)
    dialog:add_button("Delete", delete_profile, 2, 3, 1, 1)

    dialog:add_label("", 1, 4, 2, 1)

    dialog:add_label("<center><h3>Settings</h3></center>", 1, 5, 2, 1)

    dialog:add_label("Profile name:", 1, 6, 1, 1)
    profile_name_input = dialog:add_text_input("", 2, 6, 1, 1)

    dialog:add_label("Opening start (s):", 1, 7, 1, 1)
    opening_start_time_input = dialog:add_text_input("", 2, 7, 1, 1)

    dialog:add_label("Opening stop (s):", 1, 8, 1, 1)
    opening_stop_time_input = dialog:add_text_input("", 2, 8, 1, 1)

    dialog:add_label("", 1, 9, 2, 1)

    dialog:add_label("Ending start (s):", 1, 10, 1, 1)
    ending_start_time_input = dialog:add_text_input("", 2, 10, 1, 1)

    dialog:add_label("Ending stop (s):", 1, 11, 1, 1)
    ending_stop_time_input = dialog:add_text_input("", 2, 11, 1, 1)

    dialog:add_button("Save", save_profile, 1, 12, 2, 1)

    populate_profile_dropdown()
    populate_profile_fields()

    dialog:show()
end

function populate_profile_dropdown()
    profile_dropdown = dialog:add_dropdown(1, 2, 2, 1)

    for i, profile in pairs(profiles) do
        profile_dropdown:add_value(profile.name, i)
    end
end

function populate_profile_fields()
    local profile = profiles[profile_dropdown:get_value()]

    if profile then
        profile_name_input:set_text(profile.name)
        opening_start_time_input:set_text(profile.opening_start_time)
        opening_stop_time_input:set_text(profile.opening_stop_time)
        ending_start_time_input:set_text(profile.ending_start_time)
        ending_stop_time_input:set_text(profile.ending_stop_time)
    end
end

function delete_profile()
    local dropdown_value = profile_dropdown:get_value()

    if profiles[dropdown_value] then
        profiles[dropdown_value] = nil
        save_all_profiles()
    end
end

function save_profile()
    if profile_name_input:get_text() == "" then
        return
    end
    if opening_start_time_input:get_text() == "" then
        opening_start_time_input:set_text("0")
    end
    if opening_stop_time_input:get_text() == "" then
        opening_stop_time_input:set_text("0")
    end
    if ending_start_time_input:get_text() == "" then
        ending_start_time_input:set_text("0")
    end
    if ending_stop_time_input:get_text() == "" then
        ending_stop_time_input:set_text("0")
    end

    local updated_existing = false

    for _, profile in pairs(profiles) do
        if profile.name == profile_name_input:get_text() then
            profile.opening_start_time = tonumber(opening_start_time_input:get_text())
            profile.opening_stop_time = tonumber(opening_stop_time_input:get_text())
            profile.ending_start_time = tonumber(ending_start_time_input:get_text())
            profile.ending_stop_time = tonumber(ending_stop_time_input:get_text())
            updated_existing = true
        end
    end

    if not updated_existing then
        table.insert(profiles, {
            name = profile_name_input:get_text(),
            opening_start_time = tonumber(opening_start_time_input:get_text()),
            opening_stop_time = tonumber(opening_stop_time_input:get_text()),
            ending_start_time = tonumber(ending_start_time_input:get_text()),
            ending_stop_time = tonumber(ending_stop_time_input:get_text())
        })
    end

    save_all_profiles()
end

function playing_changed()
    activate(true)
    if vlc.input.is_playing() then
        if set_profile_based_on_video_name(vlc.input.item():name()) then
            local input = vlc.object.input()
            local opening = true;
            local ending = true;

            if opening_start_time_input:get_text() == "" or opening_stop_time_input:get_text() == "" then
                opening = false
            end
            if ending_start_time_input:get_text() == "" or ending_stop_time_input:get_text() == "" then
                ending = false
            end

            local opening_start = tonumber(opening_start_time_input:get_text())
            local opening_stop = tonumber(opening_stop_time_input:get_text())
            local ending_start = tonumber(ending_start_time_input:get_text())
            local ending_stop = tonumber(ending_stop_time_input:get_text())
            local duration = vlc.input.item():duration()

            while (vlc.playlist.status() == "playing") do
                local time = (vlc.var.get(input, "time") / 1000000)
                if opening then
                    if (time > opening_start and time < opening_stop) then
                        vlc.var.set(input, "position", (opening_stop + 0.01) / duration)
                    end
                end
                if ending then
                    if (time > ending_start and time < ending_stop) then
                        vlc.var.set(input, "position", (ending_stop + 0.01) / duration)
                    end
                end
                vlc.keep_alive()
                sleep(0.5)
            end
        end
    end
end

function sleep(s)
    local ntime = os.clock() + s / 10
    repeat
    until os.clock() > ntime
end

function set_profile_based_on_video_name(name)
    for _, profile in pairs(profiles) do
        if string.find(name, profile.name) then
            -- profile_dropdown:set_value(i) // Doesn't exist in VLC Api
            profile_name_input:set_text(profile.name)
            opening_start_time_input:set_text(profile.opening_start_time)
            opening_stop_time_input:set_text(profile.opening_stop_time)
            ending_start_time_input:set_text(profile.ending_start_time)
            ending_stop_time_input:set_text(profile.ending_stop_time)
            return true;
        end
    end
    vlc.msg.dbg("There is not profile for " .. name .. "!")
    return false;
end

function save_all_profiles()
    io.output(config_file)
    for _, profile in pairs(profiles) do
        io.write(profile.name)
        io.write("=")
        io.write(profile.opening_start_time)
        io.write(",")
        io.write(profile.opening_stop_time)
        io.write(",")
        io.write(profile.ending_start_time)
        io.write(",")
        io.write(profile.ending_stop_time)
        io.write("\n")
    end
    io.close()

    dialog:del_widget(profile_dropdown)
    populate_profile_dropdown()
end

function load_all_profiles()
    local lines = lines_from(config_file)

    for _, line in pairs(lines) do
        for name, opening_start_time, opening_stop_time, ending_start_time, ending_stop_time in
            string.gmatch(line, "(.+)=(%d+),(%d+),(%d+),(%d+)") do
            table.insert(profiles, {
                name = name,
                opening_start_time = opening_start_time,
                opening_stop_time = opening_stop_time,
                ending_start_time = ending_start_time,
                ending_stop_time = ending_stop_time
            })
        end
    end
end

function file_exists(file)
    local f = io.open(file, "rb")
    if f then
        f:close()
    end
    return f ~= nil
end

function lines_from(file)
    local lines = {}

    for line in io.lines(file) do
        lines[#lines + 1] = line
    end

    return lines
end
